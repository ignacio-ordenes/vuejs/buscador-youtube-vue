import YoutubeRepository from './EntityRepository/YoutubeRepository'

const repositories = {
    youtube: YoutubeRepository
};

const RepositoryFactory = {
    get: name => repositories[name]
};

export default RepositoryFactory;